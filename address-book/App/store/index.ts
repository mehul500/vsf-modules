import { Module } from 'vuex'
import actions from './actions'
import RootState from '@vue-storefront/core/types/RootState'
import AddressBookState from '../types/AddressBookState'

export const addressBookModule: Module<AddressBookState, RootState> = {
  namespaced: true,
  state: {
    customer: []
  },
  actions
}
